<?php
if (!defined('PROJECT_NAME')) {
	define('PROJECT_NAME', 'portal');
}

if (!defined('URL')) {
	define('URL', 'http://192.168.0.110/'.PROJECT_NAME.'/');
}

if (!defined('ADMIN_URL')) {
	define('ADMIN_URL', URL.'admin/');
}

if (!defined('SERVER_NAME')) {
	define('SERVER_NAME', $_SERVER['SERVER_NAME']);
}

if (!defined('ADMIN_FOLDER_NAME')) {
	define('ADMIN_FOLDER_NAME', 'admin');
}


if (!defined('DOC_ROOT')) {
	if($_SERVER['SERVER_NAME'] == 'rojan' or $_SERVER['SERVER_NAME'] == '192.168.0.107')
	define('DOC_ROOT', str_replace('/','\\',$_SERVER['DOCUMENT_ROOT']));
	else
	{
		define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT']);	
	}
}

if (!defined('ASSET_FOLDER')) {
	if($_SERVER['SERVER_NAME'] == 'rojan' or $_SERVER['SERVER_NAME'] == '192.168.0.107')
	define('ASSET_FOLDER', str_replace('/','\\',$_SERVER['DOCUMENT_ROOT']).'\\himalayan\\assets\\');
	else
	{
		define('ASSET_FOLDER', $_SERVER['DOCUMENT_ROOT'].'/assets/');	
	}
}

if (!defined('UPLOADS_FOLDER')) {
	if($_SERVER['SERVER_NAME'] == 'rojan' or $_SERVER['SERVER_NAME'] == '192.168.0.107')
	define('UPLOADS_FOLDER', str_replace('/','\\',$_SERVER['DOCUMENT_ROOT']).'\\himalayan\\assets\\uploads\\');
	else
	{
		define('UPLOADS_FOLDER', $_SERVER['DOCUMENT_ROOT'].'/assets/uploads/');	
	}
}


if (!defined('DEFAULT_CONTROLLER')) {
	define('DEFAULT_CONTROLLER', 'login');
}

if (!defined('DEFAULT_ACTION')) {
	define('DEFAULT_ACTION', 'index');
}

if (!defined('DEFAULT_ADMIN_CONTROLLER')) {
	define('DEFAULT_ADMIN_CONTROLLER', 'rabbits');
}

if (!defined('DEFAULT_ADMIN_ACTION')) {
	define('DEFAULT_ADMIN_ACTION', 'index');
}

if (!defined('LOGO')) {
	define('LOGO', 'images/logo.png');
}

if (!defined('ASSET_URL')) {
	define('ASSET_URL', URL.'assets/');
}

if (!defined('UPLOAD_URL')) {
	define('UPLOAD_URL', URL.'assets/uploads/');
}

if (!defined('CAPTCHA_PUBLIC_KEY')) {
	define('CAPTCHA_PUBLIC_KEY', '6LdQ4_gSAAAAAPh5QFnu83_u6s_bh9a2HDchWXqj');
}

if (!defined('CAPTCHA_PRIVATE_KEY')) {
	define('CAPTCHA_PRIVATE_KEY', '6LdQ4_gSAAAAAD-TerNJXUwEAXUWCrWI-1Uxi0Hy');
}

if (!defined('INVENTORY_MANAGER')) {
	define('INVENTORY_MANAGER', '1');
}

if(!defined('1')){
	define('1','INVENTORY_MANAGER');
}

if(!defined('2')){
	define('2','DEPARTMENT_MANAGER');
}

if(!defined('3')){
	define('3','ASSISTANT');
}

if(!defined('4')){
	define('4','PURCHASE_MANAGER');
}

if (!defined('DEPARTMENT_MANAGER')) {
	define('DEPARTMENT_MANAGER', '2');
}
if (!defined('ASSISTANT')) {
	define('ASSISTANT', '3');
}
if (!defined('PURCHASE_MANAGER')) {
	define('PURCHASE_MANAGER', '4');
}
if (!defined('ADMIN')) {
	define('ADMIN', 'admin');
}

if (!defined('ERROR')) {
	define('ERROR', 'err');
}

if (!defined('CONFLICT')) {
	define('CONFLICT', 'conflict');
}

if(!defined('NOCONFLICT')){
	define('NOCONFLICT', 'no conflict');	
}

?>