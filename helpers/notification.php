<?php
if(!function_exists('notifications'))
{
	function notifications()
	{
		$notifications= getModel('notification')->getNotification();
		//var_dump($notifications);die;
		if($notifications != false)
		{
			$notification_array = array();
			foreach ($notifications as $notification) {
				$type = strtolower($notification['type']);
				$notification1 = getModel('notification')->getNotificationDetail($type, $notification['notification_id']);
				$notification1['type'] = $type;
				if ($notification1['type'] == "eta")
				{
					$date = strtotime(date('m/d/Y'));
					$nDate = strtotime($notification1["etaDate"]);
					if ($date < $nDate)
					{
						break;
					}
				}
				array_push($notification_array, $notification1);
			}
			return $notification_array;
		}
	}


	function notificationStockById($item_id)
	{
		$notifications= getModel('notification')->getNotification();
		if($notifications)
		{
			$notification_array = array();
			$notificationStock = getModel('notification')->getNotificationDetailById('stock', $item_id);
			array_push($notification_array, $notificationStock);

			return $notification_array;
		}

	}


	function notificationStock($dept_id=false)
	{
		$notifications= getModel('notification')->getNotification();
		if($notifications)
		{
			$notification_array = array();
			foreach ($notifications as $notification) {
				$type = strtolower($notification['type']);
				if ($type == "stock")
				{
					if($dept_id)
					{
						$notificationStock = getModel('notification')->getNotificationDetail($type, $notification['notification_id'], $dept_id, false);
					}
					else
					{
						$notificationStock = getModel('notification')->getNotificationDetail($type, $notification['notification_id']);
					}
					if($notificationStock)
						array_push($notification_array, $notificationStock);
				}

			}

			return $notification_array;
		}
	}

	function notificationPurchase($item_id = false)
	{
		$notifications= getModel('notification')->getNotification();
		if($notifications != false)
		{
			$notification_array = array();
			foreach ($notifications as $notification) {
				$type = strtolower($notification['type']);
				if ($type == "purchase")
				{
					if($item_id){
						$notificationStock = getModel('notification')->getNotificationDetail($type, $notification['notification_id'],false, $item_id);
					}
					else{
						$notificationStock = getModel('notification')->getNotificationDetail($type, $notification['notification_id']);
					}
					//$notificationStock['type'] = $type;
					if($notificationStock)
						array_push($notification_array, $notificationStock);
				}

			}
			// echo"<pre>";
			// var_dump($notification_array);
			return $notification_array;
		}
	}

	function notificationETA()
	{
		
		$notifications= getModel('notification')->getNotification();
		if($notifications != false)
		{
			$notification_array = array();
			foreach ($notifications as $notification) {
				$type = strtolower($notification['type']);
				if ($type == "eta")
				{
					$notificationStock = getModel('notification')->getNotificationDetail($type, $notification['notification_id']);
					$notificationStock['type'] = $type;
					array_push($notification_array, $notificationStock);
				}

			}
			return $notification_array;
		}
	}
}
?>