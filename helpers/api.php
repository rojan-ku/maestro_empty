<?php

if(!function_exists('respond'))
{
	 function respond($response, $status_code = 200, $header_message = 'OK') {
		$code = 200;
		$message = 'OK';
		if (isset($status_code)) {
			$code = $status_code;
		}
		if (isset($header_message)) {
			$message = $header_message;
		}
		header('HTTP/1.1 ' . $code . ' ' . $message, true, $code);
		if (isset($response)) {
			echo json_encode($response);
		}
		exit(0);

	}

}