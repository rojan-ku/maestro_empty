<?php
// if (!defined('PDO::ATTR_DRIVER_NAME')) {
// echo 'PDO unavailable'; die;
// }
// //require_once 'config/config.php';
// $db = new PDO('mysql:host=localhost;dbname=maestro;charset=utf8','root' , '');
// try {
//     //connect as appropriate as above
//     $db->query('SELECT * FROM table1'); //invalid query!
// } catch(PDOException $ex) {
//     echo "An Error occured!"; //user friendly message
//     var_dump($ex->getMessage());
// }
// foreach($db->query('SELECT * FROM table1') as $row) {
//     echo $row['name'].' '.$row['value1']; //etc...
// }

// var_dump($db);die;
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('America/Los_Angeles');
ob_start();
session_start();
session_regenerate_id();
foreach (glob("config/*.php") as $configs) {
    include $configs;
}
require_once 'system/controller.php';
require_once 'system/apicontroller.php';
require_once 'system/model.php';
require_once 'helpers/Autoload.php';
require_once 'system/widget.php';
require_once 'system/class.phpmailer.php';




foreach (glob("controllers/*.php") as $controllers) {
    include $controllers;
}

foreach (glob("admin/controllers/*.php") as $adminControllers) {
    include $adminControllers;
}

foreach (glob("api/controllers/*.php") as $adminControllers) {
    include $adminControllers;
}

$router = new route();


set_time_limit(0);

