<?php

require_once('system/connection.php');
class Session
{

	private static $instance = NULL;
	public static $connection;


	private function __construct()
	{
		$this->connection = Connection::GetInstance();
	}

	public function GetInstance()
	{

		if(!self::$instance)
		{
			
			self::$instance = new Session();
		}
		return self::$instance;
	}



	public static function createNewSession($member)
	{
		$date = date('Y-m-d');
		$expiry_time = strtotime(date('Y-m-d', strtotime('+2 days')));	
		$sql = "DELETE FROM user_session where user_id = ".$member->user_id;
		self::GetInstance()->connection->DeleteQuery($sql);
		$sessionstore="INSERT INTO  user_session (user_id,user_name,user_access_level,expire) values ('".$member->user_id."','".$member->user_name."','".$member->user_access_level."','".$expiry_time."')";
		$sessdata=self::GetInstance()->connection->InsertQuery($sessionstore);
			//need to check if the data is inserted into the session table and then only apple the condition to initialze the sessionvariable value
		self::addSessionId($sessdata);
		self::create_session($member);

		return true;
	}
	
	public static function session_start($type)
	{
		session_start();	
	}

	// CREATE SESSION
	public static function create_session($user)
	{
		$_SESSION['user_firstname'] = $user->user_firstname;
		$_SESSION['user_lastname'] = $user->user_lastname;
		$_SESSION['user_type'] = $user->user_type;
		$_SESSION['user_email'] = $user->user_email;
		$_SESSION['user_picture'] = $user->user_picture;
		$_SESSION['user_code'] = $user->user_code;
	}

	public static function getCurrentSession()
	{
		self::GetInstance();
		$token = self::getSessionId();
		$sessionv="Select * from user_session join users on user_session.user_id = users.user_id where session_id='".$token."'";
		$ses_cur_val = self::GetInstance()->connection->Query($sessionv);

		//$ses_cur_val=$this->connection->Query($sessionv);
		if($ses_cur_val)
			return $ses_cur_val[0];
		else return false;
	}

	public static function getSessionId()
	{
		if(isset($_SESSION['session_id']))
			return $_SESSION['session_id'];
		else return false;
	}

	public static function addSessionId($token)
	{
		$_SESSION['session_id'] = $token;
	}

	
	public static function session_close()
	{
		$sessdeldata=false;
		if(isset($_SESSION['session_id']))
		{
			$currentsession=$_SESSION['session_id'];
			$sessiondelete="DELETE FROM  user_session  WHERE session_id='".$currentsession."'";
			$sessdeldata=self::GetInstance()->connection->DeleteQuery($sessiondelete);
			unset($_SESSION['session_id']);

		}
		return $sessdeldata;

	}

	public static function isLoggedIn()
	{
		$token = self::getSessionId();
		$session = self::getCurrentSession($token);
		
		if($session)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public static function addSessionVariable($key, $value)
	{
		if(isset($key) and isset($value))
		{
			$_SESSION[$key] = $value;
		}
	}

	public function getSessionVariable($key)
	{
		if(isset($key) and isset($_SESSION[$key]))
		{
			return $_SESSION[$key];
		}
	}

	public static function addErrorMessage($msg)
	{
		if(!isset($_SESSION['error'])) $_SESSION['error'] = array();
		array_push($_SESSION['error'], $msg);
	}

	public static function showErrorMessages()
	{
		if(isset($_SESSION['error']))
		{
			include 'views/default/error-messages.phtml';
		}
	}

	public function addSuccessMessage($msg)
	{
		if(!isset($_SESSION['success'])) $_SESSION['success'] = array();
		array_push($_SESSION['success'], $msg);
	}

	public static function showSuccessMessages()
	{
		if(isset($_SESSION['success']))
		{
			include 'views/default/success-messages.phtml';
		}
	}
}
?>