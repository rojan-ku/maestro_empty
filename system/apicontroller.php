<?php
class APIController extends Controller
{
	public function __construct()
	{
		parent::__construct();
	}


	protected function sendResponse($response, $status_code = 200, $status_message = "OK")
	{
		$code = 200;
		$message = 'OK';
		if (isset($status_code)) {
			$code = $status_code;
		}
		if (isset($status_message)) {
			$message = $status_message;
		}
		header('HTTP/1.1 ' . $code . ' ' . $message, true, $code);
		if (isset($response)) {
			echo json_encode($response);
		}
		exit(0);
	}
}