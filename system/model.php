<?php

require_once 'connection.php';
class Model
{
	public $connection;
	public $table;
	public $alias = null;
	public $query;
	public $where;
	public $idName;
	public $idValue = null;
	public $order = false;

	public  function __construct()
	{
		$this->connection = Connection::GetInstance();
		$this->where = false;
		if(is_null($this->alias))
			$this->alias = $this->table;

		$sql = "SHOW KEYS FROM ".$this->table." WHERE Key_name = 'PRIMARY'";
		$result = $this->connection->Query($sql);
		if($result)
			$this->idName = $result[0]['Column_name'];
		else exit("Error! No Primary Key found");


		$sql = "SHOW COLUMNS FROM ".$this->table;
		$result = $this->connection->Query($sql);
		if (!$result) {
			echo 'Could not run query: ' . mysql_error();
			exit;
		}

		foreach($result as $field)
		{
			$this->{$field['Field']} = NULL;
		}

		//var_dump($this->album_title);die;

		//echo $idName;
	}

	public function str_replace_first($from, $to, $subject)
	{
		$from = '/'.preg_quote($from, '/').'/';

		return preg_replace($from, $to, $subject, 1);
	}

	public function getCollection()
	{
		if(is_null($this->alias)) $this->alias = $this->table;
		$this->query = "Select * From ".$this->table;
		$this->where = false;
		return $this;
	}

	public function select($column_name = "*")
	{
		if(!is_array($column_name))
			$this->query = $this->str_replace_first("Select * From", "Select ".$column_name." From", $this->query);
		else
		{
			$columns = "";
			foreach ($column_name as $c) {
				$columns .= $c;
				$columns .= ", ";
			}
			$columns = rtrim($columns, ", ");
			$this->query = $this->str_replace_first("Select * From", "Select ".$columns." From", $this->query);

		}
		return $this;
	}

	public function selectwrap ($column_name = "*")
	{
		if(!is_array($column_name))
		{
			$this->query = "SELECT $column_name FROM (".$this->query.") AS ".$this->alias;
		}
		else
		{
			$columns = "";
			foreach ($column_name as $c) {
				$columns .= $c;
				$columns .= ", ";
			}
			$columns = rtrim($columns, ", ");
			$this->query = "SELECT $columns FROM (".$this->query.") AS ".$this->alias;
		}
		return $this;
	}

	public function addOperator($column_name, $value, $type ='=')
	{
		switch ($type) {
			case '=':
			$this->query .= $column_name." = '".$value."'";
			break;				
			case '>':
			$this->query .= $column_name." > '".$value."'";
			break;				
			case '>=':
			$this->query .= $column_name." >= '".$value."'";
			break;				
			case '<':
			$this->query .= $column_name." < '".$value."'";
			break;				
			case '<=':
			$this->query .= $column_name." <= '".$value."'";
			break;				
			case '!=':
			$this->query .= $column_name." != '".$value."'";
			break;				
			case 'Like':
			case 'like':
			$this->query .= $column_name." Like '".$value."'";
			break;
			case '%Like%':
			case '%like%':
			$this->query .= $column_name." Like '%".$value."%'";
			break;
			case 'NULL':
			case 'null':
			$this->query .= $column_name." IS NULL";
			break;						
			case '!NULL':
			case '!null':
			$this->query .= $column_name." IS NOT NULL";							
			break;
			default:
					# code...
			break;
		}
	}
	public function andFilter($column_name, $value, $type = '=')
	{
		if($this->where == false)
		{
			$this->query .= " Where ";
			$this->addOperator($column_name, $value, $type);
			$this->where = true;
		}
		else
		{
			$this->query .= " AND ";
			$this->addOperator($column_name, $value, $type);
		}

		return $this;
	}

	public function orFilter($column_name, $value, $type = '=')
	{
		if($this->where == false)
		{
			$this->query .= " Where ";
			$this->addOperator($column_name, $value, $type);
			$this->where = true;
		}
		else
		{
			$this->query .= " OR ";
			$this->addOperator($column_name, $value, $type);
		}
		return $this;
	}

	public function findInSet($value, $column_name)
	{
		if($this->where == false)
		{
			$this->query .= " Where ";
			$this->query .= "FIND_IN_SET('".$value."',".$column_name.")";
			$this->where = true;
		}
		else
		{
			$this->query.= " AND FIND_IN_SET('".$value."',".$column_name.")";			
		}
		return $this;
	}

	public function order($column_name, $type = "ASC", $forced = false)
	{
		if($this->order == false || $forced == true)
		{
			$this->query .= " ORDER BY ".$column_name." ".$type;			
			$this->order = true;
		}
		else
		{
			$this->query .= ", ".$column_name." ".$type;
		}
		return $this;
	}
	public function group($column_name)
	{
		$this->query .= " Group By ".$column_name;
		return $this;
	}

	public function join($model, $table1Column, $table2Column, $table1Columns = '*', $table2Columns = '*')
	{

		$table1Select = '';
		$table2Select = '';

		if(is_array($table1Columns))
		{
			foreach($table1Columns as $column)
			{
				$table1Select .= $this->alias.'.'.$column.',';
			}
			$table1Select = rtrim($table1Select, ',');
		}
		else
		{
			$table1Select = '*';
		}

		if(is_array($table2Columns))
		{
			foreach($table2Columns as $column)
			{
				$table2Select .= $model->alias.'.'.$column.',';
			}
			$table2Select = rtrim($table2Select, ',');
		}
		else $table2Select = '*';

		$tableSelect = $table1Select.','.$table2Select;
		if($tableSelect == '*,*')
		{
			$tableSelect = '*';
		}


		$this->query = "Select ".$tableSelect." From ((".$this->query.") as ".$this->alias." join (".$model->query.") as ".$model->alias." ON ".$this->alias.".".$table1Column." = ".$model->alias.".".$table2Column.")";
		return $this;
	}

	public function leftjoin($model, $table1Column, $table2Column, $table1Columns = '*', $table2Columns = '*')
	{

		$table1Select = '';
		$table2Select = '';

		if(is_array($table1Columns))
		{
			foreach($table1Columns as $column)
			{
				$table1Select .= $this->alias.'.'.$column.',';
			}
			$table1Select = rtrim($table1Select, ',');
		}
		else
		{
			$table1Select = '*';
		}

		if(is_array($table2Columns))
		{
			foreach($table2Columns as $column)
			{
				$table2Select .= $model->alias.'.'.$column.',';
			}
			$table2Select = rtrim($table2Select, ',');
		}
		else $table2Select = '*';

		$tableSelect = $table1Select.','.$table2Select;
		if($tableSelect == '*,*')
		{
			$tableSelect = '*';
		}


		$this->query = "Select ".$tableSelect." From ((".$this->query.") as ".$this->alias." left join (".$model->query.") as ".$model->alias." ON ".$this->alias.".".$table1Column." = ".$model->alias.".".$table2Column.")";
		return $this;
	}

	public function rightjoin($model, $table1Column, $table2Column, $table1Columns = '*', $table2Columns = '*')
	{

		$table1Select = '';
		$table2Select = '';

		if(is_array($table1Columns))
		{
			foreach($table1Columns as $column)
			{
				$table1Select .= $this->alias.'.'.$column.',';
			}
			$table1Select = rtrim($table1Select, ',');
		}
		else
		{
			$table1Select = '*';
		}

		if(is_array($table2Columns))
		{
			foreach($table2Columns as $column)
			{
				$table2Select .= $model->alias.'.'.$column.',';
			}
			$table2Select = rtrim($table2Select, ',');
		}
		else $table2Select = '*';

		$tableSelect = $table1Select.','.$table2Select;
		if($tableSelect == '*,*')
		{
			$tableSelect = '*';
		}


		$this->query = "Select ".$tableSelect." From ((".$this->query.") as ".$this->alias." right join (".$model->query.") as ".$model->alias." ON ".$this->alias.".".$table1Column." = ".$model->alias.".".$table2Column.")";
		return $this;
	}



	public function limit($value = '*', $offset = '0')
	{
		if($value != '*')
			$this->query .= " Limit ".$value." Offset ".$offset;

		return $this;
	}

	public function load($id)
	{
		$this->query = "SELECT * FROM ".$this->table." WHERE ".$this->idName." = '".$id."' LIMIT 1";
		$result = $this->execute()[0];
		$this->idValue = $id;
		foreach($result as $key => $value)
		{
			$this->$key = $value;
		}

		return $this;
	}

	public function loadByField($id, $value)
	{
		$this->query = "SELECT * FROM ".$this->table." WHERE ".$id." = '".$value."' LIMIT 1";
		
		$result = $this->execute()[0];
		$this->idValue = $result[$this->idName];
		foreach($result as $key => $value)
		{
			$this->$key = $value;
		}

		return $this;
	}
	public function setData($data = array())
	{
		foreach($data as $key => $value)
		{
			$this->$key = $value;
		}
		//var_dump($this->getData());
		return $this;
	}

	public function isDatabaseField($key, $includeID = true)
	{
		if($key == "connection" || $key == 'table' || $key == 'alias' || $key == 'query' || $key == 'where' || $key == 'idName' || $key == 'idValue' || $key == $this->idName || $key == "order")
		{
			return false;
		}
		else return true;
	}

	public function getData()
	{
		$variables = get_object_vars($this);
		$data = array();
		foreach($variables as $key => $value)
		{
			//echo $key;
			if($this->isDatabaseField($key))
			{
				$data[$key] = $value;
			}
		}
		return $data;
	}

	public function save()
	{
		if(is_null($this->idValue))
		{
			$variables = get_object_vars($this);
			$data = array();
			$values = ' VALUES (';
				$this->query = "INSERT INTO ".$this->table." (";
					foreach($variables as $key => $value)
					{
			//echo $key;
						if($this->isDatabaseField($key, false))
						{
				//$data[$key] = $value;
							if(!is_null($value))
							{
								$this->query .= $key.', ';
								// var_dump($value);
								// var_dump($this->connection->db->quote($value));echo '<br>';
								$values .= "".$this->getNullString($this->connection->db->quote($value)).", ";
								// var_dump($values);die;						
							}
						}
					}
					$values = rtrim($values, ', ');
					$values .= ')';
$this->query = rtrim($this->query, ', ');
$this->query .= ")";
$this->query .= $values;
$lastInsertID = $this->connection->InsertQuery($this->query);
$this->load($lastInsertID);

}
else
{
	$variables = get_object_vars($this);
	$data = array();
	$this->query = "UPDATE ".$this->table." SET ";
	foreach($variables as $key => $value)
	{
		if($this->isDatabaseField($key, false))
		{
			if(!is_null($value))
			{
				$this->query .= $key." = ".$this->getNullString($this->connection->db->quote($value)).", ";
			}
		}
	}

	$this->query = rtrim($this->query, ', ');
	$this->query .= " WHERE ".$this->idName." = '".$this->idValue."'";
	$this->connection->UpdateQuery($this->query);
}

return $this;
}

public function getNullString($val)
{
	if(is_null($val))
	{
		return NULL;
	}
	else return $val;
}

public function lastInsertID()
{
	$this->query = "SELECT MAX(".$this->idName." ) as id FROM ".$this->table;
	$result = $this->execute()[0];
	return $result;
}

public function delete()
{
	if(!is_null($this->idName) && !is_null($this->idValue))
	{
		$this->query = "DELETE FROM ".$this->table." WHERE ".$this->idName." = '".$this->idValue."'";
		$this->connection->DeleteQuery($this->query);
		$this->idName = null;
		$this->idValue = null;
	}
}

public function deleteAll($column, $value)
{
	$this->query = "DELETE FROM ".$this->table." WHERE ".$column." = '".$value."'";
	$this->connection->DeleteQuery($this->query);
}

public function execute()
{
	return $this->connection->Query($this->query);
}

public function getQuery()
{
	echo $this->query.'<br/>';
}
}
?>