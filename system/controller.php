<?php
require_once 'system/view.php';
class Controller
{
	protected $view;
	protected $model;
	protected $facebook;
	function __construct()
	{
		$this->facebook = false;
		if(USE_FB_LOGIN == true)
		{
			require_once 'helpers/facebook.php';
			$this->facebook = new Facebook(array(
				'appId'  => FB_APPID,
				'secret' => FB_APP_SECRET
				));
			
		}
		if($this->facebook != false)
		{

			$this->view = new View($this->facebook);
		}
		else
		{
			$this->view = new View();

		}

		$this->mail             = new PHPMailer();

		$this->mail->IsSMTP(); // telling the class to use SMTP
		                                           // 1 = errors and messages
		                                           // 2 = messages only
		$this->mail->SMTPAuth   = true;                  // enable SMTP authentication
		$this->mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
		$this->mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$this->mail->Port       = 465;                   // set the SMTP port for the GMAIL server
		$this->mail->Username   = "info@eddietechnologies.com";  // GMAIL username
		$this->mail->Password   = "EF2j$J1m";            // GMAIL password
	}

	public function pagenotfoundAction()
	{
		$this->view->render('default/404.phtml');
	}
}
